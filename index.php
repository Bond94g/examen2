<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Мебельная компания");
?>
<?php if ($APPLICATION->GetCurPage(false) != '/'):?>
    <p>Если вы используете базовую редакцию продукта - «Компания», то можете перейти на редакцию «Совместная работа», и
        ваш портал приобретет все возможности для осуществления горизонтальных коммуникаций и управлению Экстранетом.
        Вы сможете организовать внутри компании коллективную работу над проектами в рабочих группах, вести учет и
        планирование времени и событий, обмениваться сообщениями и почтой через портал, проводить внутри компании
        видеоконференции
        и делать многое другое.</p>
    <p>Можете сразу перейти с «младшей», базовой редакции на «Бизнес-процессы». После такого перехода ваш портал можно
        будет интегрировать с внешним сайтом, на портале добавятся возможности визуального проектирования
        бизнес-процессов
        и управления списками, включая списки в рабочих группах. Кроме того, вы сможете анализировать посещаемость
        своего портала</p>
    <p>При переходе вся информация на портале сохранится. Вам не потребуется создавать сайт заново. С помощью технологии
        SiteUpdate вы получите новые модули продукта и установите их без помощи разработчиков.</p>
    <h2>Выставка новых образцов</h2>
    <hr>
    <div class="article-text-block">
        <figure class="pic-block">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/pic01.jpg" alt="">
        </figure>
        <p>Ваш портал приобретет все возможности для осуществления горизонтальных коммуникаций и управлению Экстранетом.
            Вы сможете организовать внутри компании коллективную работу над проектами в рабочих группах, вести учет и
            планирование
            времени и событий, обмениваться сообщениями и почтой через портал, проводить внутри компании
            видеоконференции и делать многое другое.</p>
        <h3>Заголовок</h3>
        <p><i>Можете сразу перейти с «младшей», базовой редакции на «Бизнес-процессы». После такого перехода ваш портал
                можно будет интегрировать с внешним сайтом, на портале добавятся возможности визуального проектирования
                бизнес-процессов и управления списками, включая списки в рабочих группах. Кроме того, вы сможете
                анализировать посещаемость своего портала</i>
        </p>
        <p><span class="att-text">При переходе вся информация на портале сохранится.</span>
            <br>Вам не потребуется создавать сайт заново. С помощью технологии SiteUpdate вы получите новые модули
            продукта и установите их без помощи разработчиков.</p>
    </div>
<?php else:?>
    <p>«Мебельная компания» осуществляет производство мебели на высококлассном оборудовании с применением минимальной доли ручного труда, что позволяет обеспечить высокое качество нашей продукции. Налажен производственный процесс как массового и индивидуального характера, что с одной стороны позволяет обеспечить постоянную номенклатуру изделий и индивидуальный подход – с другой.
    </p>


    <!-- index column -->
    <div class="cnt-section index-column">
        <div class="col-wrap">

            <!-- main actions box -->
            <div class="column main-actions-box">
                <div class="title-block">
                    <h2>Новинки</h2>
                    <hr>
                </div>
                <div class="items-wrap">
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title-block att">
                                Угловой диван!
                            </div>
                            <br>
                            <div class="inner-block">
                                <a href="" class="photo-block">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/new01.jpg" alt="">
                                </a>
                                <div class="text"><a href="">Угловой диван "Титаник",  с большим выбором расцветок и фактур.</a>
                                    <a href="" class="btn-arr"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title-block att">
                                Угловой диван!
                            </div>
                            <br>
                            <div class="inner-block">
                                <a href="" class="photo-block">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/new02.jpg" alt="">
                                </a>
                                <div class="text"><a href="">Угловой диван "Титаник",  с большим выбором расцветок и фактур.</a>
                                    <a href="" class="btn-arr"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title-block att">
                                Угловой диван!
                            </div>
                            <br>
                            <div class="inner-block">
                                <a href="" class="photo-block">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/new03.jpg" alt="">
                                </a>
                                <div class="text"><a href="">Угловой диван "Титаник",  с большим выбором расцветок и фактур.</a>
                                    <a href="" class="btn-arr"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="" class="btn-next">Все новинки</a>
            </div>
            <!-- /main actions box -->
            <!-- main news box -->
            <div class="column main-news-box">
                <div class="title-block">
                    <h2>Новости</h2>
                </div>
                <hr>
                <div class="items-wrap">
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title"><a href="">29 августа 2012</a>
                            </div>
                            <div class="text"><a href="">Поступление лучших офисных кресел из Германии </a>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title"><a href="">29 августа 2012</a>
                            </div>
                            <div class="text"><a href="">Мастер-класс дизайнеров  из Италии для производителей мебели </a>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title"><a href="">29 августа 2012</a>
                            </div>
                            <div class="text"><a href="">Поступление лучших офисных кресел из Германии </a>
                            </div>
                        </div>
                    </div>
                    <div class="item-wrap">
                        <div class="item">
                            <div class="title"><a href="">29 августа 2012</a>
                            </div>
                            <div class="text"><a href="">Наша дилерская сеть расширилась теперь ассортимент наших товаров доступен в Казани </a>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="" class="btn-next">Все новости</a>
            </div>
            <!-- /main news box -->

        </div>
    </div>
    <!-- /index column -->

    <!-- afisha box -->
    <div class="cnt-section afisha-box">
        <div class="section-title-block">
            <h2 class="second-ttile">Ближайшие мероприятия</h2>
            <a href="" class="btn-next">все мероприятия</a>
        </div>
        <hr>
        <div class="items-wrap">
            <div class="item-wrap">
                <div class="item">
                    <div class="title"><a href="">29 августа 2012, Москва</a>
                    </div>
                    <div class="text"><a href="">Семинар производителей мебели России и СНГ, Обсуждение тенденций.</a>
                    </div>
                </div>
            </div>
            <div class="item-wrap">
                <div class="item">
                    <div class="title"><a href="">29 августа 2012, Москва</a>
                    </div>
                    <div class="text"><a href="">Открытие шоу-рума на Невском проспекте. Последние модели в большом ассортименте.</a>
                    </div>
                </div>
            </div>
            <div class="item-wrap">
                <div class="item">
                    <div class="title"><a href="">29 августа 2012, Москва</a>
                    </div>
                    <div class="text"><a href="">Открытие нового магазина в нашей  дилерской сети.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /afisha box -->
<?php endif;?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>