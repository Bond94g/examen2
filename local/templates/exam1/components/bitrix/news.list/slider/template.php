<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="item-wrap">
    <div class="rew-footer-carousel">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="item-wrap">
                <div class="rew-footer-carousel">
                    <div class="item">
                        <div class="side-block side-opin">
                            <div class="inner-block">
                                <div class="title">
                                    <div class="photo-block">
                                        <? if ($arItem['PREVIEW_PICTURE']): ?>
                                            <?
                                            $arImageFile = CFile::ResizeImageGet(
                                                $arItem['PREVIEW_PICTURE']['ID'],
                                                ['width' => 39, 'height' => 39],
                                                BX_RESIZE_IMAGE_EXT,
                                                true
                                            );
                                            $PictureSrc = $arImageFile['src'];
                                            ?>
                                        <? else: ?>
                                            <?
                                            $PictureSrc = SITE_TEMPLATE_PATH . '/img/rew/no_photo_left_block.jpg';
                                            ?>
                                        <? endif; ?>
                                        <img src="<?= $PictureSrc ?>" alt="">
                                    </div>
                                    <div class="name-block"><a
                                                href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a>
                                    </div>
                                    <div class="pos-block"><? echo $arItem["DISPLAY_PROPERTIES"]["POSITION"]["DISPLAY_VALUE"] ?>
                                        ,"<? echo $arItem["DISPLAY_PROPERTIES"]["COMPANY"]["DISPLAY_VALUE"] ?>"
                                    </div>
                                </div>
                                <div class="text-block"><? echo $arItem["PREVIEW_TEXT"] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>